# Contributor: j.r <j.r@jugendhacker.de>
# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Maria Lisina <sekoohaka.sarisan@gmail.com>
pkgname=telegram-tdlib
pkgver=1.8.34
pkgrel=0
# they don't tag releases
_gitrev=1fa2a372a88c26369dcac2ce476166531df74a77
pkgdesc="Cross-platform library for building Telegram clients"
url="https://core.telegram.org/tdlib"
arch="all !s390x" # fails to build on big-endian
license="BSL-1.0"
makedepends="cmake gperf linux-headers openssl-dev samurai zlib-dev"
subpackages="$pkgname-static $pkgname-dev"
source="td-$_gitrev.tar.gz::https://github.com/tdlib/td/archive/$_gitrev.tar.gz"
builddir="$srcdir/td-$_gitrev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr

	cmake --build build
}

check() {
	ctest --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
544b74e42c998f0f94f6780743d7bf49cab9a1e4c1ef35655ecbda30d903422ee29923469e3242d5ce8dcdbf76c4bc7b801e5755a5d695c1303d36346352bd4d  td-1fa2a372a88c26369dcac2ce476166531df74a77.tar.gz
"
